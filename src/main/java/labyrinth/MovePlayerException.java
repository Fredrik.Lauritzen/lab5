package labyrinth;

public class MovePlayerException extends RuntimeException {
	private static final long serialVersionUID = -6244594256969039528L;

	public MovePlayerException(String message) {
		super(message);
	}
	
	
	
}
